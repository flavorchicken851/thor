package threads.thor.services;

import android.content.Context;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicConnection;

import java.net.Inet6Address;
import java.net.InetAddress;

import threads.LogUtils;
import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

public class LocalConnectService {

    private static final String TAG = LocalConnectService.class.getSimpleName();

    public static void connect(@NonNull Context context, @NonNull PeerId peerId,
                               @NonNull InetAddress inetAddress, int port) {

        try {
            IPFS ipfs = IPFS.getInstance(context);

            String pre = "/ip4";
            if (inetAddress instanceof Inet6Address) {
                pre = "/ip6";
            }

            String multiAddress = pre + inetAddress + "/udp/" + port + "/quic";
            Multiaddr multiaddr = new Multiaddr(multiAddress);

            QuicConnection conn = ipfs.dial(peerId, multiaddr, IPFS.CONNECT_TIMEOUT,
                    IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
            ipfs.swarmEnhance(conn);

            LogUtils.error(TAG, "Success " + peerId.toBase58() + " " + multiAddress);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

}

